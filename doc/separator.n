'\" separator.n,v 1.5 2006/11/27 05:45:02 jenglish Exp
'\"
'\" Copyright (c) 2004 Joe English
'\"
.so man.macros
.TH separator n 0.5 tile "Tile Widget Set"
.BS
.SH NAME
ttk::separator \- Separator bar
.SH SYNOPSIS
\fBttk::separator\fR \fIpathName \fR?\fIoptions\fR?
.BE
.SH DESCRIPTION
A \fBseparator\fR widget displays a horizontal or vertical separator bar.
.SO
\-class	\-cursor	\-state	\-style	
\-takefocus
.SE
.SH "OPTIONS"
.OP \-orient orient Orient
One of \fBhorizontal\fR or \fBvertical\fR.
Specifies the orientation of the separator.
.SH "WIDGET COMMAND"
Separator widgets support the standard 
\fBcget\fR, \fBconfigure\fR, \fBinstate\fR, and \fBstate\fR 
methods.  No other widget methods are used.
.SH "SEE ALSO"
widget(n)
.SH "KEYWORDS"
widget, separator
