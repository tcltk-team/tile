<manpage cat="cmd" id="combobox" title="combobox" version="0.4">
<!-- This file was automatically generated.  DO NOT EDIT (yet)! -->
<!-- 
     Copyright (c) 2004 Joe English
 -->
<?TROFF.TH title="combobox" section="n" date="0.4" source="tile" manual="Tile Widget Set" ?>
<namesection>
<name>ttk::combobox</name>
<desc>text field with popdown selection list</desc>
</namesection>
<synopsis>
<syntax>
<b>ttk::combobox</b> <m>pathName</m> ?<m>options</m>?
</syntax>
</synopsis>
<section>
<title>STANDARD OPTIONS</title>
<sl cat="stdopt" cols="4">
<li>-class</li>
<li>-cursor</li>
<li>-style</li>
<li>-takefocus</li>
</sl>
</section>
<section>
<title>OPTIONS</title>
<optionlist>
<optiondef>
<name>-exportselection</name>
<dbname>exportSelection</dbname>
<dbclass>ExportSelection</dbclass>
<desc>Boolean value.
If set, the widget selection is linked to the X selection.
</desc>
</optiondef>
<optiondef>
<name>-justify</name>
<dbname>justify</dbname>
<dbclass>Justify</dbclass>
<desc>Specifies how the text is aligned within the widget.
One of <b>left</b>, <b>center</b>, or <b>right</b>.
</desc>
</optiondef>
<optiondef>
<name>-height</name>
<dbname>height</dbname>
<dbclass>Height</dbclass>
<desc>Specifies the height of the pop-down listbox, in rows.
</desc>
</optiondef>
<optiondef>
<name>-postcommand</name>
<dbname>postCommand</dbname>
<dbclass>PostCommand</dbclass>
<desc>A Tcl script to evaluate immediately before displaying the listbox.
The <b>-postcommand</b> script may specify the <b>-values</b> to display.
</desc>
</optiondef>
<optiondef>
<name>-state</name>
<dbname>state</dbname>
<dbclass>State</dbclass>
<desc>One of <b>normal</b>, <b>readonly</b>, or <b>disabled</b>.
In the <b>readonly</b> state,
the value may not be edited directly, and 
the user can only select one of the <b>-values</b> from the
dropdown list.
In the <b>normal</b> state, 
the text field is directly editable.
In the <b>disabled</b> state, no interaction is possible.
</desc>
</optiondef>
<optiondef>
<name>-textvariable</name>
<dbname>textVariable</dbname>
<dbclass>TextVariable</dbclass>
<desc>Specifies the name of a variable whose value is linked 
to the widget value.
Whenever the variable changes value the widget value is updated,
and vice versa.
</desc>
</optiondef>
<optiondef>
<name>-values</name>
<dbname>values</dbname>
<dbclass>Values</dbclass>
<desc>Specifies the list of values to display in the drop-down listbox.
</desc>
</optiondef>
<optiondef>
<name>-width</name>
<dbname>width</dbname>
<dbclass>Width</dbclass>
<desc>Specifies an integer value indicating the desired width of the entry window,
in average-size characters of the widget's font.
</desc>
</optiondef>
</optionlist>
</section>
<section>
<title>DESCRIPTION</title>
<p>A combobox combines a text field with a pop-down list of values;
the user may select the value of the text field from among the 
values in the list.
</p>
</section>
<section>
<title>WIDGET COMMAND</title>
<dl>
<dle>
<dt><m>pathName</m> <b>cget</b> <m>option</m></dt>
<dd>Returns the current value of the specified <i>option</i>.
See <i>widget(n)</i>.
</dd>
</dle>
<dle>
<dt><m>pathName</m> <b>configure</b> ?<m>option</m>? ?<m>value option value ...</m>?</dt>
<dd>Modify or query widget options.
See <i>widget(n)</i>.
</dd>
</dle>
<dle>
<dt><m>pathName</m> <b>current</b> ?<m>newIndex</m>?</dt>
<dd>If <i>newIndex</i> is supplied, sets the combobox value 
to the element at position <i>newIndex</i> in the list of <b>-values</b>.
Otherwise, returns the index of the current value in the list of <b>-values</b>
or <b>-1</b> if the current value does not appear in the list.
</dd>
</dle>
<dle>
<dt><m>pathName</m> <b>get</b></dt>
<dd>Returns the current value of the combobox.
</dd>
</dle>
<dle>
<dt><m>pathName</m> <b>identify</b> <m>x y</m></dt>
<dd>Returns the name of the element at position <i>x</i>, <i>y</i>,
or the empty string if the coordinates are outside the window.
</dd>
</dle>
<dle>
<dt><m>pathName</m> <b>instate</b> <m>statespec</m> ?<m>script</m>?</dt>
<dd>Test the widget state.
See <i>widget(n)</i>.
</dd>
</dle>
<dle>
<dt><m>pathName</m> <b>set</b> <m>value</m></dt>
<dd>Sets the value of the combobox to <i>value</i>.
</dd>
</dle>
<dle>
<dt><m>pathName</m> <b>state</b> ?<m>stateSpec</m>?</dt>
<dd>Modify or query the widget state.
See <i>widget(n)</i>.
</dd>
</dle>
</dl>
<p>The combobox widget also supports the following <i>entry</i>
widget commands (see <i>entry(n)</i> for details):
</p>
<?TABSTOPS 5.5c 11c?>
<sl cols="3">
<li>bbox</li>
<li>delete</li>
<li>icursor</li>
<li>index</li>
<li>insert</li>
<li>selection</li>
<li>xview</li>
</sl>
</section>
<section>
<title>VIRTUAL EVENTS</title>
<p>The combobox widget generates a <b>&lt;&lt;ComboboxSelected&gt;&gt;</b> virtual event
when the user selects an element from the list of values.
This event is generated after the listbox is unposted.
</p>
</section>
<seealso>
<ref>widget(n)</ref>
<ref>entry(n)</ref>
</seealso>
</manpage>
