'\"
'\" Copyright (c) 2004 Joe English
'\"
.so man.macros
.TH menubutton n 0.2 tile "Tile Widget Set"
.BS
.SH NAME
ttk::menubutton \- Widget that pops down a menu when pressed
.SH SYNOPSIS
\fBttk::menubutton\fR \fIpathName \fR?\fIoptions\fR?
.BE
.SH DESCRIPTION
A \fBmenubutton\fR widget displays a textual label and/or image,
and displays a menu when pressed.
.SO
\-class	\-compound	\-cursor	\-image
\-state	\-style	\-takefocus	\-text
\-textvariable	\-underline	\-width
.SE
.SH "OPTIONS"
.OP \-direction direction Direction
Specifies where the menu is to be popped up relative 
to the menubutton.  
One of: \fIabove\fR, \fIbelow\fR, \fIleft\fR, \fIright\fR,
or \fIflush\fR.  The default is \fIbelow\fR.  
\fIflush\fR pops the menu up directly over the menubutton.
.OP \-menu menu Menu
Specifies the path name of the menu associated with the menubutton.
To be on the safe side, the menu ought to be a direct child of the
menubutton.
.\" not documented: may go away:
.\" .OP \-anchor anchor Anchor
.\" .OP \-padding padding Pad
.SH "WIDGET COMMAND"
Menubutton widgets support the standard 
\fBcget\fR, \fBconfigure\fR, \fBinstate\fR, and \fBstate\fR 
methods.  No other widget methods are used.
.SH "SEE ALSO"
widget(n), keynav(n), menu(n)
.SH "KEYWORDS"
widget, button, menu
