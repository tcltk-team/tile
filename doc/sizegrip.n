'\" sizegrip.n,v 1.5 2007/10/16 00:59:23 jenglish Exp
'\"
'\" Copyright (c) 2006 Joe English
'\"
.so man.macros
.TH sizegrip n 0.8 tile "Tile Widget Set"
.BS
.SH NAME
ttk::sizegrip \- A silly widget
.SH SYNOPSIS
\fBttk::sizegrip\fR \fIpathName \fR?\fIoptions\fR?
.BE
.SH DESCRIPTION
A \fBsizegrip\fR widget (also known as a \fIgrow box\fR)
allows the user to resize the containing toplevel window
by pressing and dragging the grip.
.SO
\-class	\-cursor	\-state	\-style
\-takefocus
.SE
.SH "WIDGET COMMAND"
Sizegrip widgets support the standard
\fBcget\fR, \fBconfigure\fR, \fBinstate\fR, and \fBstate\fR
methods.  No other widget methods are used.
.SH "PLATFORM-SPECIFIC NOTES"
On Mac OSX, toplevel windows automatically include a built-in
size grip by default.
Adding an \fBttk::sizegrip\fR there is harmless, since
the built-in grip will just mask the widget.
.SH EXAMPLES
.CS
# Using pack:
pack [ttk::frame $top.statusbar] -side bottom -fill x
pack [ttk::sizegrip $top.statusbar.grip] -side right -anchor se

# Using grid:
grid [ttk::sizegrip $top.statusbar.grip] \
    -row $lastRow -column $lastColumn -sticky se
# ... optional: add vertical scrollbar in $lastColumn,
# ... optional: add horizontal scrollbar in $lastRow
.CE
.SH "BUGS"
If the containing toplevel's position was specified
relative to the right or bottom of the screen
(e.g., \fB[wm geometry ... \fIw\fBx\fIh\fB-\fIx\fB-\fIy\fB]\fR
instead of \fB[wm geometry ... \fIw\fBx\fIh\fB+\fIx\fB+\fIy\fB]\fR),
the sizegrip widget will not resize the window.
.PP
ttk::sizegrip widgets only support "southeast" resizing.
.SH "SEE ALSO"
widget(n)
.SH "KEYWORDS"
widget, sizegrip, grow box
