'\" Copyright (c) 2005 Joe English
.so man.macros
.TH labelframe n 0.7 tile "Tile Widget Set"
.BS
.SH NAME
ttk::labelframe \- Container widget with optional label
.SH SYNOPSIS
\fBttk::labelframe\fR \fIpathName \fR?\fIoptions\fR?
.BE
.SH DESCRIPTION
A \fBlabelframe\fR widget is a container used to group other widgets together.
It has an optional label, which may be a plain text string or another widget. 
.SO
\-class	\-cursor	\-takefocus	\-style
.SE
.SH "OPTIONS"
'\" XXX: Currently included, but may go away: 
'\" XXX: .OP -borderwidth borderWidth BorderWidth
'\" XXX: The desired width of the widget border.  Default is theme-dependent.
'\" XXX: .OP -relief relief Relief
'\" XXX: One of the standard Tk border styles:
'\" XXX: \fBflat\fR, \fBgroove\fR, \fBraised\fR, \fBridge\fR,
'\" XXX: \fBsolid\fR, or \fBsunken\fR.
'\" XXX: Default is theme-dependent.
.OP -labelanchor labelAnchor LabelAnchor
Specifies where to place the label.  
Allowed values are (clockwise from the top upper left corner):
\fBnw\fR, \fBn\fR, \fBne\fR, \fBen\fR, \fBe\fR, \fBes\fR,
\fBse\fR, \fBs\fR,\fBsw\fR, \fBws\fR, \fBw\fR and \fBwn\fR.
The default value is theme-dependent.
'\" Alternate explanation: The first character must be one of n, s, e, or w
'\" and specifies which side the label should be placed on;
'\" the remaining characters specify how the label is aligned on that side.
'\" NOTE: Now allows other values as well; leave this undocumented for now
.OP -text text Text
Specifies the text of the label.
.OP -underline underline Underline
If set, specifies the integer index (0-based) of a character to 
underline in the text string.
The underlined character is used for mnemonic activation
(see \fIkeynav(n)\fR).  
Mnemonic activation for a \fBttk::labelframe\fR 
sets the keyboard focus to the first child of the \fBttk::labelframe\fR widget.
.OP -padding padding Padding
Additional padding to include inside the border.
.OP -labelwidget labelWidget LabelWidget
The name of a widget to use for the label.
If set, overrides the \fB-text\fR option.
The \fB-labelwidget\fR must be a child of the \fBlabelframe\fR widget
or one of the \fBlabelframe\fR's ancestors, and must belong to the
same top-level widget as the \fBlabelframe\fR.
.OP -width width Width
If specified, the widget's requested width in pixels.
.OP -height height Height
If specified, the widget's requested height in pixels.
(See \fIttk::frame\fR for further notes on \fB-width\fR and \fB-height\fR).
.SH "WIDGET COMMAND"
Supports the standard widget commands
\fBconfigure\fR, \fBcget\fR, \fBinstate\fR, and \fBstate\fR;
see \fIwidget(n)\fR.
.SH "SEE ALSO"
widget(n), frame(n)
.SH "KEYWORDS"
widget, frame, container, label, groupbox
