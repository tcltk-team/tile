'\"
'\" Copyright (c) 2004 Joe English
'\"
.so man.macros
.TH radiobutton n 0.2 tile "Tile Widget Set"
.BS
.SH NAME
ttk::radiobutton \- Mutually exclusive option widget
.SH SYNOPSIS
\fBttk::radiobutton\fR \fIpathName \fR?\fIoptions\fR?
.BE
.SH DESCRIPTION
\fBradiobutton\fR widgets are used in groups to show or change
a set of mutually-exclusive options.
Radiobuttons are linked to a Tcl variable,
and have an associated value; when a radiobutton is clicked,
it sets the variable to its associated value.
.SO
\-class	\-compound	\-cursor	\-image
\-state	\-style	\-takefocus	\-text
\-textvariable	\-underline	\-width
.SE
.SH "OPTIONS"
.OP \-command command Command
A Tcl script to evaluate whenever the widget is invoked.
.OP \-value Value Value
The value to store in the associated \fI-variable\fR 
when the widget is selected. 
.OP \-variable variable Variable
The name of a global variable whose value is linked to the widget.
Default value is \fB::selectedButton\fR.
.SH "WIDGET COMMAND"
In addition to the standard 
\fBcget\fR, \fBconfigure\fR, \fBinstate\fR, and \fBstate\fR 
commands, radiobuttons support the following additional
widget commands:
.TP
\fIpathname\fR invoke
Sets the \fI-variable\fR to the \fI-value\fR, selects the widget,
and evaluates the associated \fI-command\fR.  
Returns the result of the \fI-command\fR, or the empty
string if no \fI-command\fR is specified.
.\" Missing: select, deselect.  Useful?
.\" Missing: flash.  This is definitely not useful.
.SH "WIDGET STATES"
The widget does not respond to user input if the \fBdisabled\fR state is set.
The widget sets the \fBselected\fR state whenever 
the linked \fB-variable\fR is set to the widget's \fB-value\fR,
and clears it otherwise.
The widget sets the \fBalternate\fR state whenever the 
linked \fB-variable\fR is unset.  
(The \fBalternate\fR state may be used to indicate a ``tri-state'' 
or ``indeterminate'' selection.)
.SH "SEE ALSO"
widget(n), keynav(n), checkbutton(n)
.SH "KEYWORDS"
widget, button, option
