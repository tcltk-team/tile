'\"
'\" Copyright (c) 2004 Joe English
'\"
.so man.macros
.TH checkbutton n 0.2 tile "Tile Widget Set"
.BS
.SH NAME
ttk::checkbutton \- On/off widget
.SH SYNOPSIS
\fBttk::checkbutton\fR \fIpathName \fR?\fIoptions\fR?
.BE
.SH DESCRIPTION
A \fBcheckbutton\fR widget is used to show or change a setting.
It has two states, selected and deselected.  
The state of the checkbutton may be linked to a Tcl variable.
.SO
\-class	\-compound	\-cursor	\-image
\-state	\-style	\-takefocus	\-text
\-textvariable	\-underline	\-width
.SE
.SH "OPTIONS"
.OP \-command command Command
A Tcl script to execute whenever the widget is invoked.
.OP \-offvalue offValue OffValue
The value to store in the associated \fI-variable\fR 
when the widget is deselected.  Defaults to \fB0\fR.
.OP \-onvalue onValue OnValue
The value to store in the associated \fI-variable\fR 
when the widget is selected.  Defaults to \fB1\fR.
.OP \-variable variable Variable
The name of a global variable whose value is linked to the widget.
Defaults to the widget pathname if not specified.
.SH "WIDGET COMMAND"
In addition to the standard 
\fBcget\fR, \fBconfigure\fR, \fBinstate\fR, and \fBstate\fR 
commands, checkbuttons support the following additional
widget commands:
.TP
\fIpathname\fR invoke
Toggles between the selected and deselected states
and evaluates the associated \fI-command\fR.
If the widget is currently selected, sets the \fI-variable\fR
to the \fI-offvalue\fR and deselects the widget;
otherwise, sets the \fI-variable\fR to the \fI-onvalue\fR
Returns the result of the \fI-command\fR.
.\" Missing: select, deselect, toggle 
.\" Are these useful?  They don't invoke the -command
.\" Missing: flash.  This is definitely not useful.
.SH "WIDGET STATES"
The widget does not respond to user input if the \fBdisabled\fR state is set.
The widget sets the \fBselected\fR state whenever 
the linked \fB-variable\fR is set to the widget's \fB-onvalue\fR,
and clears it otherwise.
The widget sets the \fBalternate\fR state whenever the 
linked \fB-variable\fR is unset.  
(The \fBalternate\fR state may be used to indicate a ``tri-state'' 
or ``indeterminate'' selection.)
.SH "SEE ALSO"
widget(n), keynav(n), radiobutton(n)
.SH "KEYWORDS"
widget, button, toggle, check, option
