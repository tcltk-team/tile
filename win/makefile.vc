# makefile.vc --                                               -*- Makefile -*-
#
# Microsoft Visual C++ makefile for use with nmake.exe v1.62+ (VC++ 5.0+)
#
# This makefile is based upon the Tcl 8.4 Makefile.vc and modified to 
# make it suitable as a general package makefile. Look for the word EDIT
# which marks sections that may need modification. As a minumum you will
# need to change the PROJECT, DOTVERSION and DLLOBJS variables to values
# relevant to your package.
#
# See the file "license.terms" for information on usage and redistribution
# of this file, and for a DISCLAIMER OF ALL WARRANTIES.
# 
# Copyright (c) 1995-1996 Sun Microsystems, Inc.
# Copyright (c) 1998-2000 Ajuba Solutions.
# Copyright (c) 2001 ActiveState Corporation.
# Copyright (c) 2001-2002 David Gravereaux.
# Copyright (c) 2003-2006 Pat Thoyts
#
#-------------------------------------------------------------------------
# RCS: @(#)makefile.vc,v 1.61 2007/12/16 18:20:55 jenglish Exp
#-------------------------------------------------------------------------

!if !defined(MSDEVDIR) && !defined(MSVCDIR) && !defined(VCToolkitInstallDir)
MSG = ^
You will need to run vcvars32.bat from Developer Studio, first, to setup^
the environment.  Jump to this line to read the new instructions.
!error $(MSG)
!endif

#------------------------------------------------------------------------------
# HOW TO USE this makefile:
#
# 1)  It is now necessary to have %MSVCDir% set in the environment.  This is
#     used  as a check to see if vcvars32.bat had been run prior to running
#     nmake or during the installation of Microsoft Visual C++, MSVCDir had
#     been set globally and the PATH adjusted.  Either way is valid.
#
#     You'll need to run vcvars32.bat contained in the MsDev's vc(98)/bin
#     directory to setup the proper environment, if needed, for your current
#     setup.  This is a needed bootstrap requirement and allows the swapping of
#     different environments to be easier.
#
# 2)  To use the Platform SDK (not expressly needed), run setenv.bat after
#     vcvars32.bat according to the instructions for it.  This can also turn on
#     the 64-bit compiler, if your SDK has it.
#
# 3)  Targets are:
#	all       -- Builds everything.
#       <project> -- Builds the project (eg: nmake sample)
#	test      -- Builds and runs the test suite.
#	install   -- Installs the built binaries and libraries to $(INSTALLDIR)
#		     in an appropriate subdirectory.
#	clean/realclean/distclean -- varying levels of cleaning.
#
# 4)  Macros usable on the commandline:
#
#	INSTALLDIR=<path>
#		Sets where to install Tcl from the built binaries.
#		C:\Progra~1\Tcl is assumed when not specified.
#
#	OPTS=static,msvcrt,staticpkg,threads,symbols,profile,loimpact,none
#		Sets special options for the core.  The default is for none.
#		Any combination of the above may be used (comma separated).
#		'none' will over-ride everything to nothing.
#
#		static  =  Builds a static library of the core instead of a
#			   dll.  The shell will be static (and large), as well.
#		msvcrt  =  Effects the static option only to switch it from
#			   using libcmt(d) as the C runtime [by default] to
#			   msvcrt(d). This is useful for static embedding
#			   support.
#		staticpkg = Effects the static option only to switch
#			   tclshXX.exe to have the dde and reg extension linked
#			   inside it.
#		nothreads = Turns off multithreading support (not recommended)
#		thrdalloc = Use the thread allocator (shared global free pool).
#		symbols =  Adds symbols for step debugging.
#		profile =  Adds profiling hooks.  Map file is assumed.
#		loimpact =  Adds a flag for how NT treats the heap to keep memory
#			   in use, low.  This is said to impact alloc performance.
#
#	Package specific options:
#		nouxtheme = Do not compile in support for WinXP themeing.
#		nostubs = Link with the Tk library. This is required if building 
#			against Tk < 8.4.6 which doesn't support all the methods
#			in the stubs table that we need.
#		square = Include the sample 'square' widget.
#
#	STATS=memdbg,compdbg,none
#		Sets optional memory and bytecode compiler debugging code added
#		to the core.  The default is for none.  Any combination of the
#		above may be used (comma separated).  'none' will over-ride
#		everything to nothing.
#
#		memdbg   = Enables the debugging memory allocator.
#		compdbg  = Enables byte compilation logging.
#
#	MACHINE=(IX86|IA64|ALPHA)
#		Set the machine type used for the compiler, linker, and
#		resource compiler.  This hook is needed to tell the tools
#		when alternate platforms are requested.  IX86 is the default
#		when not specified.
#
#	TMP_DIR=<path>
#	OUT_DIR=<path>
#		Hooks to allow the intermediate and output directories to be
#		changed.  $(OUT_DIR) is assumed to be 
#		$(BINROOT)\(Release|Debug) based on if symbols are requested.
#		$(TMP_DIR) will de $(OUT_DIR)\<buildtype> by default.
#
#	TESTPAT=<file>
#		Reads the tests requested to be run from this file.
#
#	CFG_ENCODING=encoding
#		name of encoding for configuration information. Defaults
#		to cp1252
#
# 5)  Examples:
#
#	Basic syntax of calling nmake looks like this:
#	nmake [-nologo] -f makefile.vc [target|macrodef [target|macrodef] [...]]
#
#                        Standard (no frills)
#       c:\tcl_src\win\>c:\progra~1\micros~1\vc98\bin\vcvars32.bat
#       Setting environment for using Microsoft Visual C++ tools.
#       c:\tcl_src\win\>nmake -f makefile.vc all
#       c:\tcl_src\win\>nmake -f makefile.vc install INSTALLDIR=c:\progra~1\tcl
#
#                         Building for Win64
#       c:\tcl_src\win\>c:\progra~1\micros~1\vc98\bin\vcvars32.bat
#       Setting environment for using Microsoft Visual C++ tools.
#       c:\tcl_src\win\>c:\progra~1\platfo~1\setenv.bat /pre64 /RETAIL
#       Targeting Windows pre64 RETAIL
#       c:\tcl_src\win\>nmake -f makefile.vc MACHINE=IA64
#
#------------------------------------------------------------------------------
#==============================================================================
###############################################################################
#------------------------------------------------------------------------------

!if !exist("makefile.vc")
MSG = ^
You must run this makefile only from the directory it is in.^
Please `cd` to its location first.
!error $(MSG)
!endif

#-------------------------------------------------------------------------
# Project specific information (EDIT)
#
# You should edit this with the name and version of your project. This
# information is used to generate the name of the package library and
# it's install location.
#
# For example, the sample extension is  going to build sample04.dll and
# would install it into $(INSTALLDIR)\lib\sample04
#
# You need to specify the object files that need to be linked into your
# binary here.
#
#-------------------------------------------------------------------------

PROJECT = tile
!include "rules.vc"

DOTVERSION      = 0.8.2
VERSION         = $(DOTVERSION:.=)
STUBPREFIX      = ttkstub

DLLOBJS = \
	$(TMP_DIR)\tile.obj \
	$(TMP_DIR)\ttkStubInit.obj \
	$(TMP_DIR)\tkElements.obj \
	$(TMP_DIR)\label.obj \
	$(TMP_DIR)\layout.obj \
	$(TMP_DIR)\separator.obj \
	$(TMP_DIR)\frame.obj \
	$(TMP_DIR)\button.obj \
	$(TMP_DIR)\scrollbar.obj \
	$(TMP_DIR)\progress.obj \
	$(TMP_DIR)\scale.obj \
	$(TMP_DIR)\notebook.obj \
	$(TMP_DIR)\paned.obj \
	$(TMP_DIR)\entry.obj \
	$(TMP_DIR)\treeview.obj \
	$(TMP_DIR)\widget.obj \
	$(TMP_DIR)\trace.obj \
	$(TMP_DIR)\track.obj \
	$(TMP_DIR)\blink.obj \
	$(TMP_DIR)\scroll.obj \
	$(TMP_DIR)\manager.obj \
	$(TMP_DIR)\tagset.obj \
	$(TMP_DIR)\tkstate.obj \
	$(TMP_DIR)\altTheme.obj \
	$(TMP_DIR)\classicTheme.obj \
	$(TMP_DIR)\tkTheme.obj \
	$(TMP_DIR)\cache.obj \
	$(TMP_DIR)\clamTheme.obj \
	$(TMP_DIR)\image.obj \
	$(TMP_DIR)\winTheme.obj \
	$(TMP_DIR)\xpTheme.obj \
	$(TMP_DIR)\monitor.obj \
	$(TMP_DIR)\tile.res

PRJSTUBOBJS = $(TMP_DIR)\ttkStubLib.obj

# Special make options.
!if [nmakehlp -f $(OPTS) "nostubs"]
NOSTUBS = 1
!endif

#-------------------------------------------------------------------------
# Target names and paths ( shouldn't need changing )
#-------------------------------------------------------------------------

BINROOT		= .
ROOT            = ..

!ifdef NOSTUBS
NS              =ns
TMP_DIR         =$(TMP_DIR)_NoStubs
OUT_DIR         =$(OUT_DIR)_NoStubs
!else
NS              =
!endif

PRJIMPLIB	= $(OUT_DIR)\$(PROJECT)$(VERSION)$(SUFX).lib
PRJLIBNAME	= $(PROJECT)$(VERSION)$(NS)$(SUFX).$(EXT)
PRJLIB		= $(OUT_DIR)\$(PRJLIBNAME)

PRJSTUBLIBNAME	= $(STUBPREFIX).lib
PRJSTUBLIB	= $(OUT_DIR)\$(PRJSTUBLIBNAME)
PRJHEADERS      = $(GENERICDIR)\tkTheme.h

### Make sure we use backslash only.
PRJ_INSTALL_DIR         = $(_INSTALLDIR)\$(PROJECT)$(DOTVERSION)
LIB_INSTALL_DIR		= $(PRJ_INSTALL_DIR)
BIN_INSTALL_DIR		= $(PRJ_INSTALL_DIR)
DOC_INSTALL_DIR		= $(PRJ_INSTALL_DIR)
SCRIPT_INSTALL_DIR	= $(PRJ_INSTALL_DIR)
DEMO_INSTALL_DIR	= $(PRJ_INSTALL_DIR)
INCLUDE_INSTALL_DIR	= $(_TCLDIR)\include

### The following paths CANNOT have spaces in them.
GENERICDIR	= $(ROOT)\generic
WINDIR		= $(ROOT)\win
LIBDIR          = $(ROOT)\library
DOCDIR		= $(ROOT)\doc
TOOLSDIR	= $(ROOT)\tools
COMPATDIR	= $(ROOT)\compat

#---------------------------------------------------------------------
# Compile flags
#---------------------------------------------------------------------

!if !$(DEBUG)
!if $(OPTIMIZING)
### This cranks the optimization level to maximize speed
cdebug	= $(OPTIMIZATIONS)
!else
cdebug	=
!endif
!else if "$(MACHINE)" == "IA64"
### Warnings are too many, can't support warnings into errors.
cdebug	= -Z7 -Od -GZ
!else
cdebug	= -Z7 -WX -Od -GZ
!endif

### Declarations common to all compiler options
cflags = -nologo -c -YX -Fp$(TMP_DIR)^\

# Warning level
!if $(FULLWARNINGS)
cflags = $(cflags) -W4
!else
cflags = $(cflags) -W3
!endif

!if $(PENT_0F_ERRATA)
cflags = $(cflags) -QI0f
!endif

!if $(ITAN_B_ERRATA)
cflags = $(cflags) -QIA64_Bx
!endif

!if $(MSVCRT)
!if $(DEBUG) && !$(UNCHECKED)
crt = -MDd
!else
crt = -MD
!endif
!else
!if $(DEBUG) && !$(UNCHECKED)
crt = -MTd
!else
crt = -MT
!endif
!endif

INCLUDES        = $(TCL_INCLUDES) $(TK_INCLUDES) \
		  -I"$(WINDIR)" -I"$(GENERICDIR)"
BASE_CFLAGS	= $(cflags) $(cdebug) $(crt) $(INCLUDES)
CON_CFLAGS	= $(cflags) $(cdebug) $(crt) -DCONSOLE
TCL_CFLAGS	= -DUSE_TCL_STUBS \
		  -DVERSION="\"$(DOTVERSION)\"" $(BASE_CFLAGS) $(OPTDEFINES)

!ifndef NOSTUBS
TCL_CFLAGS      = $(TCL_CFLAGS) -DUSE_TK_STUBS
!endif

!if ![nmakehlp -f $(OPTS) "nouxtheme"]
TCL_CFLAGS     = $(TCL_CFLAGS) -DHAVE_UXTHEME_H
!endif

!if [nmakehlp -f $(OPTS) "square"]
DLLOBJS        = $(DLLOBJS) $(TMP_DIR)\square.obj
TCL_CFLAGS     = $(TCL_CFLAGS) -DTTK_SQUARE_WIDGET
!endif


#---------------------------------------------------------------------
# Link flags
#---------------------------------------------------------------------

!if $(DEBUG)
ldebug	= -debug:full -debugtype:cv
!else
ldebug	= -release -opt:ref -opt:icf,3
!endif

### Declarations common to all linker options
lflags	= -nologo -machine:$(MACHINE) $(ldebug)

!if $(PROFILE)
lflags	= $(lflags) -profile
!endif

!if $(ALIGN98_HACK) && !$(STATIC_BUILD)
### Align sections for PE size savings.
lflags	= $(lflags) -opt:nowin98
!else if !$(ALIGN98_HACK) && $(STATIC_BUILD)
### Align sections for speed in loading by choosing the virtual page size.
lflags	= $(lflags) -align:4096
!endif

!if $(LOIMPACT)
lflags	= $(lflags) -ws:aggressive
!endif

dlllflags = $(lflags) -dll
conlflags = $(lflags) -subsystem:console
guilflags = $(lflags) -subsystem:windows

!ifdef NOSTUBS
baselibs   = $(TCLSTUBLIB) $(TKIMPLIB) user32.lib gdi32.lib
!else
baselibs   = $(TCLSTUBLIB) $(TKSTUBLIB) user32.lib gdi32.lib
!endif

#---------------------------------------------------------------------
# TclTest flags
#---------------------------------------------------------------------

!IF "$(TESTPAT)" != ""
TESTFLAGS = -file $(TESTPAT)
!ENDIF

#---------------------------------------------------------------------
# Project specific targets (EDIT)
#---------------------------------------------------------------------

all:	    setup $(PROJECT)
$(PROJECT): setup $(PRJLIB) $(PRJSTUBLIB)

install:    install-binaries install-libraries install-docs install-demos


test: setup $(PROJECT)
	set TILE_LIBRARY=$(ROOT)/library
!if "$(OS)" == "Windows_NT"  || "$(MSVCDIR)" == "IDE"
	$(TCLSH) <<
	package require Tk
	set env(TILE_LIBRARY) [file normalize [file join $(ROOT) library]]
	load [file join [file normalize {$(OUT_DIR)}] $(PRJLIBNAME)] Tile
	set argv $(TESTFLAGS)
	source [file normalize [file join $(ROOT) tests all.tcl]]
<<
!else
	@echo Please wait while the tests are collected...
	$(TCLSH) "$(ROOT)/tests/all.tcl" $(TESTFLAGS) > tests.log
	type tests.log | more
!endif

demo: setup $(PROJECT)
	$(WISH) <<
	set env(TILE_LIBRARY) [file normalize [file join $(ROOT) library]]
	load [file join [file normalize {$(OUT_DIR)}] $(PRJLIBNAME)] Tile
	source [file join [file normalize {$(ROOT)}] demos demo.tcl]
<<


shell: setup $(PROJECT)
	$(WISH) <<
	set env(TILE_LIBRARY) [file normalize [file join $(ROOT) library]]
	load [file join [file normalize {$(OUT_DIR)}] $(PRJLIBNAME)] Tile
	console show
<<

setup:
	@if not exist $(OUT_DIR)\nul mkdir $(OUT_DIR)
	@if not exist $(TMP_DIR)\nul mkdir $(TMP_DIR)

$(PRJLIB): $(DLLOBJS)
	$(link32) $(dlllflags) -out:$@ $(baselibs) @<<
$**
<<
	-@del $*.exp

$(PRJSTUBLIB): $(PRJSTUBOBJS)
	$(lib32) -nologo -out:$@ $(PRJSTUBOBJS)

#---------------------------------------------------------------------
# Implicit rules
#---------------------------------------------------------------------

{$(WINDIR)}.c{$(TMP_DIR)}.obj::
    $(cc32) $(TCL_CFLAGS) -DBUILD_$(PROJECT) -Fo$(TMP_DIR)\ @<<
$<
<<

{$(GENERICDIR)}.c{$(TMP_DIR)}.obj::
    $(cc32) $(TCL_CFLAGS) -DBUILD_$(PROJECT) -Fo$(TMP_DIR)\ @<<
$<
<<

{$(COMPATDIR)}.c{$(TMP_DIR)}.obj::
    $(cc32) $(TCL_CFLAGS) -DBUILD_$(PROJECT) -Fo$(TMP_DIR)\ @<<
$<
<<

{$(WINDIR)}.rc{$(TMP_DIR)}.res:
	$(rc32) -fo $@ -r -i "$(GENERICDIR)" -D__WIN32__ \
		-DCOMMAVERSION=$(DOTVERSION:.=,),0 \
		-DDOTVERSION=\"$(DOTVERSION)\" \
		-DVERSION=\"$(VERSION)$(SUFX)\" \
!if $(DEBUG)
	-d DEBUG \
!endif
!if $(TCL_THREADS)
	-d TCL_THREADS \
!endif
!if $(STATIC_BUILD)
	-d STATIC_BUILD \
!endif
	$<

.SUFFIXES:
.SUFFIXES:.c .rc

#---------------------------------------------------------------------
# Installation. (EDIT)
#
# You may need to modify this section to reflect the final distribution
# of your files and possibly to generate documentation.
#
#---------------------------------------------------------------------

install-binaries:
	@echo Installing binaries to '$(SCRIPT_INSTALL_DIR)'
	@if not exist "$(SCRIPT_INSTALL_DIR)\" mkdir "$(SCRIPT_INSTALL_DIR)" #"
	@$(CPY) $(PRJLIB) "$(SCRIPT_INSTALL_DIR)" > NUL
	@$(CPY) $(PRJSTUBLIB) "$(SCRIPT_INSTALL_DIR)" > NUL
	@$(CPY) $(PRJHEADERS) "$(SCRIPT_INSTALL_DIR)" > NUL

install-libraries:
	@echo Installing libraries to '$(SCRIPT_INSTALL_DIR)'
	@if exist $(LIBDIR)\nul $(CPY) $(LIBDIR)\*.tcl "$(SCRIPT_INSTALL_DIR)" >NUL
	@echo Installing package index in '$(SCRIPT_INSTALL_DIR)'
	@type << >"$(SCRIPT_INSTALL_DIR)\pkgIndex.tcl"
# This package is built with Tk stubs for 8.4.6+ but we also provide a version
# for that is linked to tk84.dll for Tk 8.4.0-8.4.5.
# The package cannot be used with Tk < 8.4
#
if {![package vsatisfies [package provide Tcl] 8.4]} {return}
if {[package vsatisfies [package provide Tcl] 8.5]
      || [package vsatisfies [info patchlevel] 8.4.6]} {
    package ifneeded $(PROJECT) $(DOTVERSION) \
        "namespace eval tile { variable library [list $$dir] };\
         load \[file join [list $$dir] $(PROJECT)$(VERSION).$(EXT)\]"
} else {
    package ifneeded $(PROJECT) $(DOTVERSION) \
        "namespace eval tile { variable library [list $$dir] };\
         load \[file join [list $$dir] $(PROJECT)$(VERSION)ns.$(EXT)\]"
}
<<

install-docs:
	@echo Installing documentation files to '$(DOC_INSTALL_DIR)'
	@if exist $(ROOT)\README.txt $(CPY) "$(ROOT)\README.txt" "$(SCRIPT_INSTALL_DIR)" >NUL
	@if exist $(ROOT)\license.terms $(CPY) "$(ROOT)\license.terms" "$(SCRIPT_INSTALL_DIR)" >NUL

install-demos:
	@echo Installing sample applications in '$(DEMO_INSTALL_DIR)\demos'
	@if not exist "$(DEMO_INSTALL_DIR)\demos" mkdir "$(DEMO_INSTALL_DIR)\demos"
	@$(CPY) "$(ROOT)\demos" "$(DEMO_INSTALL_DIR)\demos" >NUL
	@if not exist "$(DEMO_INSTALL_DIR)\demos\themes" mkdir "$(DEMO_INSTALL_DIR)\demos\themes"
	@$(CPY) "$(ROOT)\demos\themes" "$(DEMO_INSTALL_DIR)\demos\themes" >NUL
	@if not exist "$(DEMO_INSTALL_DIR)\demos\themes\blue" mkdir "$(DEMO_INSTALL_DIR)\demos\themes\blue"
	@$(CPY) "$(ROOT)\demos\themes\blue" "$(DEMO_INSTALL_DIR)\demos\themes\blue" >NUL

#---------------------------------------------------------------------
# Clean up
#---------------------------------------------------------------------

clean:
	@if exist $(TMP_DIR)\nul $(RMDIR) $(TMP_DIR)
	@if exist $(WINDIR)\version.vc del $(WINDIR)\version.vc

realclean: clean
	@if exist $(OUT_DIR)\nul $(RMDIR) $(OUT_DIR)

distclean: realclean
	@if exist $(WINDIR)\nmakehlp.exe del $(WINDIR)\nmakehlp.exe
	@if exist $(WINDIR)\nmakehlp.obj del $(WINDIR)\nmakehlp.obj
